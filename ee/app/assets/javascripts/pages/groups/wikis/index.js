import initClonePanel from '~/clone_panel';
import { initDiffStatsDropdown } from '~/init_diff_stats_dropdown';
import initWikis from '~/pages/shared/wikis';

initWikis();
initClonePanel();
initDiffStatsDropdown();
